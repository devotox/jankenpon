JanKenPon

A classic Rock Paper Scissors Game Implemented in Javascript ( Livescript )

### Commands -
	`gulp - Runs all
	gulp dev - Runs all and watches
	gulp watch - Watches all folders
	gulp test - Runs all lints / tests (if there were some)
	gulp prod - Runs all and cleans dev
	gulp templates - Compiles HTML ['html', 'jade', 'dust']
	gulp images - Compiles Images - ['jpg', 'png', 'gif', 'bmp']
	gulp styles - Compiles Styles - ['css', 'less', 'sass', 'stylus']
	gulp scripts - Compiles Scripts - ['javascript', 'livescript', 'coffeescript', 'typescript']
	gulp publish - Updates package.json & bower.json creates release, creates git tag, and publishes to npm & bower`


### Caveats -
	`Files / Folders are retrieved in alphabetical order`
