'use strict'

class JanKenPon
	defaults:
		debug: false # Sends debugging information to the console
		global: false # Sets `this` to window.JKP

		body: \body # selector for app placement
		grid_id: \grid # Sets image grid id
		rules_id: \rules # Sets rules container id
		header_id: \header # Sets header container id
		container_id: \container # Sets main container id

		rules_hide: false # Toggles rules visibility
		header_hide: false # Toggles header visibility
		header1: "Jan Ken Pon \u21ba" # Sets header H1
		header2: "Using Jade - Stylus - Livescript - Gulp" # Sets header H2
		rules1: "Choose Between Comp vs Comp, Player 1 Vs Comp, Player 1 vs Player 2"
		rules2: "Player One Controls: A - Rock, S - Paper, D - Scissors"
		rules3: "Player Two Controls: J - Rock, K - Paper, L - Scissors"

		default_game_action: \cvc
		defautlt_game_choice: \rock

		game_options: [
			{
				name: 'Comp vs Comp'
				action: 'cvc'
			}, {
				name: 'Player vs Comp'
				action: 'pvc'
			}, {
				name: 'Player vs Player'
				action: 'pvp'
			}
		]

		player_options: {
			one: {
				a: \rock
				s: \paper
				d: \scissors
			}

			two: {
				j: \rock
				k: \paper
				l: \scissors
			}
		}

		game_choices: <[ rock paper scissors ]>

		game_rules: {
			rock: {
				scissors: true
			}
			paper: {
				rock: true
			}
			scissors: {
				paper: true
			}
		}

	current_game_action: null

	components: {}

	players: {}

	(config) ~>
		@set_config config
		@create_container!
		@set_game_options!
		@create_scoreboard!
		@create_players!
		@create_button!
		@set_players!
		@set_global!
		@actions!

	run: ->	
		@winner_container!.text ''
		@reset_game_choice!
		@animate!

	check_rules: ([p1, p2]) ->
		@debug 'Current Game Action', @current_game_action
		@debug 'Player Choices', [ p1, p2 ]

		p1 or= @config.defautlt_game_choice
		p2 or= @config.defautlt_game_choice

		p1_win = @config.game_rules[p1].hasOwnProperty(p2)
		p2_win = @config.game_rules[p2].hasOwnProperty(p1)

		winner = if p1_win then \one else if p2_win then \two
		@debug 'Winner', [ winner or \draw, p1_win, p2_win ]
		@update_score winner if winner
		@alert_winner winner

	alert_winner: (winner) ->
		setTimeout ~> @components.winner_container.text if winner then "Player #{winner} wins!" else "Draw!"

	update_score: (winner) ->
		@score ?= {}
		@score[winner] ?= 0; @score[winner]++
		@components.score[winner].text @score[winner]

	set_players: ->
		@players.one = { name: \one }
		@players.two = { name: \two }

		for own i, v of @config.game_choices
			@players.one[v] = "images/left/#{v}.png"
			@players.two[v] = "images/right/#{v}.png"

		@players.one.player = $('#player1').empty().append @players.one.image = $ '<img />', src: @players.one.rock
		@players.two.player = $('#player2').empty().append @players.two.image = $ '<img />', src: @players.two.rock

	set_player_choices: ->
		choices = [
			@config.game_choices[ @get_choice @players.one ]
			@config.game_choices[ @get_choice @players.two ]
		]

		@players.one.image.attr 'src', @players.one[ choices[0] ] if choices[0]
		@players.two.image.attr 'src', @players.two[ choices[1] ] if choices[1]
		@check_rules choices

	get_choice: (player) ->		
		if @current_game_action is \cvc or ( @current_game_action is \pvc and player.name is \two )
			@random @config.game_choices.length
		else @get_player_choice player

	get_player_choice: (player) ->
		@debug 'Player Choice' [ player.name, @current_choices[player.name] ]
		@config.game_choices.indexOf? @current_choices[player.name]

	set_game_options: ->
		@components.game_options ?= {}
		@current_game_action = @config.default_game_action
		@container!.append @components.game_options.container or= $ '<div />', id: \game_options

		for own x, y of @config.game_options
			@components.game_options.container.append do
				@components.game_options[y.action] or= $ '<button />', "data-action": y.action, text: y.name, class: "game_options_btn"
			if y.action is @current_game_action then @components.game_options[y.action].css background-color: \blue, color: \white

	set_config: (config) ->
		@config ?= {}; unless config
			@config = @defaults
			return @debug 'JanKenPon Config', @config

		for own key, value of @defaults
			@config[key] = config?[key] ?= value

		@debug 'JanKenPon Config', @config

	actions: ->
		@document!.bind 'keyup.JKP', ~> @set_current_key it
		@header!.find 'h1, h2' .bind 'click.JKP', ~> @reload!
		@start_button!.bind 'click.JKP', ~> @reset_players!; @run!
		@game_options_buttons!.bind 'click.JKP', ~> @switch_current_game_action it

	set_current_key: (event) ->
		char_code = event.which
		key = String.fromCharCode char_code ?.toLowerCase?!
		@debug 'Current Key Pressed', [ key?.toUpperCase?!, char_code ]

		@current_choices ?= {}
		if @config.player_options.one.hasOwnProperty key
			@current_choices.one = @config.player_options.one[key]
		else if @config.player_options.two.hasOwnProperty key
			@current_choices.two = @config.player_options.two[key]

	switch_current_game_action: (event) ->
		@game_options_buttons!.attr 'style', ''
		$target = $(event.target).css background-color: \blue, color: \white
		@current_game_action = $target.data('action') or @current_game_action
		@debug 'Current Game Action', @current_game_action

	random: (max=3) -> Math.floor Math.random! * max
		
	animate: ->
		for i from 0 to 1 then @_animate!
		@reset_animate!

	_animate: ->
		@players.one.image.add(@players.two.image)
			.velocity( top: "-50px").velocity( top: "50px" )

	reset_animate: ->
		@players.one.image.add(@players.two.image)
			.velocity( top: "-50px").velocity({top: "0px"}, complete: ~> @set_player_choices! )

	reset_game_options: ->
		@reset_game_choice!
		@game_options_buttons!.attr \style, ''
		@components.game_options.container.empty?!
		@current_game_action = @config.default_game_action

	reset_players: ->
		@players.one.image.attr('src', @players.one.rock )
		@players.two.image.attr('src', @players.two.rock )

	reset_game_choice: ->		
		@current_choices = {}

	destroy: ->
		@grid!?.empty?!
		@body!?.empty?!
		window?.JKP = null
		@reset_game_options!
		@window!?.unbind? \.JKP
		@document!?.unbind? \.JKP
		@winner_container!?.text? ''

	reload: (config) ->
		@destroy?!
		@window!.scrollTop 0
		new JanKenPon config or= @config

	header: ->
		@components.header or= $ '<div />', id: @config.header_id
			.append $('<h1 />').text @config.header1
			.append $('<h2 />').text @config.header2
			.[if @config.header_hide then \hide else \show ]!

	rules: ->
		@components.rules or= $ '<div />', id: @config.rules_id
			.append $('<h1 />').text \RULES
			.append $('<p />').text @config.rules1
			.append $('<p />').text @config.rules2
			.append $('<p />').text @config.rules3
			.[if @config.rules_hide then \hide else \show ]!

	debug: (txt, obj) -> console.log txt, ': ', obj if @config.debug

	set_global: -> window?.JKP = @ if @config.global

	body: -> @components.body or= $ @config.body

	window: -> @components.window or= $ window

	document: -> @components.document or= $ document

	grid: -> @components.grid or= $ '<div />', id: @config.grid_id

	container: ->
		@components.container or= $ '<div />', id: @config.container_id, class: \clearfix

	start_button: ->
		@components.btn or= $ '<button />', id: \start_button, text: 'Run!'

	winner_container: ->
		@components.winner_container or= $ '<div />', id: \winner_container

	game_options_buttons: ->
		@components.game_options_buttons or= $ '.game_options_btn'

	create_container: ->
		@body!.append( @header! ).append( @container!.append(@winner_container!).append @grid! )
		@body!.append( @rules! )

	create_scoreboard: ->
		@components.score or= {}
		@components.score.container = $ '<div />', id: \score_container, class: \clearfix
		@components.score.one = $ '<div />', id: \score_one, style: "float: left", text: \0
		@components.score.two = $ '<div />', id: \score, style: "float: right", text: \0
		@components.score.container.append( @components.score.one ).append( @components.score.two )
		@grid!.append @components.score.container

	create_button: ->
		@container!.append @start_button!

	create_players: ->
		container = $ '<div />', id: \player_container, class: \clearfix
		player1 = $ '<div />', id: \player1, style: "float: left"
		player2 = $ '<div />', id: \player2, style: "float: right"
		container.append( player1 ).append( player2 )
		@grid!.append( container )

## Set to module exports or to window object
module.exports = JanKenPon if module?.exports
@JanKenPon = JanKenPon if @window
